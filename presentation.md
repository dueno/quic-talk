---
title: Understanding QUIC by examples
author: Daiki Ueno
date: 2021-09-02
sansfont: Fira Sans
monofont: Fira Mono
slide-numbers: true
header-includes:
  - \usepackage{tikz}
  - \usetikzlibrary{fit}
  - \usetikzlibrary{positioning}
  - \usetikzlibrary{shapes}
  - \setbeamertemplate{itemize items}{\textbullet}
---

## QUIC?

- Transport protocol used by HTTP/3
- Based on UDP
- Connection oriented
- Connections are multiplexed per stream
- Packets are protected by encryption

## Myths and confusions

- ~~HTTP/3 **is** QUIC~~
- ~~Full of layer violation~~?
- ~~Handshake happens for each stream~~
- ~~TLS libraries should implement QUIC, like DTLS~~?

## Goals of this talk

Explain important aspects of QUIC, by going through the source code
of a simple application program

Hopefully be a mythbuster

## Application: Echo server/client [^1]

### Client

`./cli [--coalescing=N] [--streams=M] HOST PORT CA-FILE`

0. Read lines from standard-input
1. Send it to the server
2. Receive any response from the server

### Server

`./serv HOST PORT KEY-FILE CERT-FILE`

0. Accept connection from a client
1. Receive data from the connection
2. Send back the same data to the connection

[^1]: In the talk description, _file synchronization application_ is mentioned, but it turned out to be too complex for 20 min talk :-)

## Prerequisite

- Modern C compiler
  - We use C for easier comparison with POSIX networking API
- GnuTLS 3.7.2 or later
  - Package available in Debian and Fedora
- ngtcp2
  - Embedded as a git submodule
- Meson build system
  - Provides a way to easily embed subproject
- GLib, Linux (for epoll, timerfd)

## ngtcp2

_"[ngtcp2](https://github.com/ngtcp2/ngtcp2) project is an effort to
implement RFC9000 QUIC protocol."_

Flexible QUIC library which features:

- No network I/O performed by the library itself
- TLS library agnostic

## Code setup

Code: [https://gitlab.com/dueno/quic-echo](https://gitlab.com/dueno/quic-echo)

- `meson.build`
- `cli.c`
- `serv.c`
- `connection.[ch]`
- `stream.[ch]`
- `gnutls-glue.[ch]`
- `utils.[ch]`
- `subprojects/ngtcp2`

Caveat: code excerpt on the following slides omit error checking for
readability

## Program behavior breakdown

### Client

0. _Preparation_: Resolve host, create a socket
1. _Handshake_: Establish a connection
2. _Stream opening_: Open a stream
3. _Data transfer_: Read/write data from/to the stream and stdio

### Server

0. _Preparation_: Resolve host, create a socket
1. _Handshake_: Accept a connection
2. _Stream opening_: Get notified upon stream open
3. _Data transfer_: Read/write data from/to the stream

## Preparation: Resolve and create a socket

This can be done with usual `getaddrinfo()`, `socket()`, `connect()` (or `bind()`) pattern.

\footnotesize
```c
ret = getaddrinfo (host, port, &hints, &result);
...
for (rp = result; rp != NULL; rp = rp->ai_next)
  {
    fd = socket (rp->ai_family,
                 rp->ai_socktype | SOCK_NONBLOCK,
                 rp->ai_protocol);
    if (fd >= 0 && connect (fd, rp->ai_addr, rp->ai_addrlen) == 0)
      break;
    close (fd);
  }
freeaddrinfo (result);
```

## Handshake: TCP and QUIC

::: {.columns}

:::: {.column}

\begin{tikzpicture}
\draw (0,0) -- (0,6);
\draw (5,0) -- (5,6);
\draw[->] (0,5) -- (5,5)
        node [above,near start,align=left]
        {SYN};
\draw[<-] (0,3) -- (5,3)
        node [above,near end,align=left]
        {SYN + ACK};
\draw[->] (0,1) -- (5,1)
        node [above,near start,align=left]
        {ACK};
\end{tikzpicture}

::::

:::: {.column}

\begin{tikzpicture}
\draw (0,0) -- (0,6);
\draw (5,0) -- (5,6);
\draw[->] (0,5) -- (5,5)
        node [above,near start,align=left,font=\tiny]
        {\normalsize Initial (CRYPTO)};
\draw[<-] (0,2.8) -- (5,2.8)
        node [above,near end,align=left,font=\tiny]
        {{\normalsize Initial (CRYPTO)}\\
		\textcolor{purple}{\normalsize Handshake (CRYPTO)}};
\draw[->] (0,1.5) -- (5,1.5)
        node [above,near start,align=left,font=\tiny]
        {\textcolor{purple}{\small Handshake (CRYPTO)}\\
		\\
		\textcolor{violet}{\normalsize 1-RTT*}};
\draw[<-] (0,0.5) -- (5,0.5)
        node [above,near end,align=left,font=\tiny]
        {\textcolor{violet}{\normalsize 1-RTT (HANDSHAKE\_DONE)}};
\end{tikzpicture}

::::

:::

## Handshake: QUIC and TLS

::: {.columns}

:::: {.column}

QUIC handshake _wraps_ TLS handshake in 3 packet types:

1. Initial: Encrypted with initial secret
2. \textcolor{purple}{Handshake}: Encrypted with handshake traffic secret
3. \textcolor{violet}{1-RTT}: Encrypted with application traffic secret

\

During this process, QUIC also negotiates initial transport parameters through `quic_transport_parameters` TLS extension.

::::

:::: {.column}

\begin{tikzpicture}
\draw (0,0) -- (0,6);
\draw (5,0) -- (5,6);
\draw[->] (0,5) -- (5,5)
        node [above,near start,align=left,font=\tiny]
        {\normalsize Initial (CRYPTO)\\
		{Client Hello}};
\draw[<-] (0,2.8) -- (5,2.8)
        node [above,near end,align=left,font=\tiny]
        {{\normalsize Initial (CRYPTO)}\\
		{Server Hello}\\
		\textcolor{purple}{\normalsize Handshake (CRYPTO)}\\
         \textcolor{purple}{EncryptedExtensions}\\
         \textcolor{purple}{Certificate}\\
         \textcolor{purple}{Certificate Verify}\\
         \textcolor{purple}{Finished}};
\draw[->] (0,1.5) -- (5,1.5)
        node [above,near start,align=left,font=\tiny]
        {\textcolor{purple}{\small Handshake (CRYPTO)}\\
		\textcolor{purple}{Finished}\\
		\\
		\textcolor{violet}{\normalsize 1-RTT*}};
\draw[<-] (0,0.5) -- (5,0.5)
        node [above,near end,align=left,font=\tiny]
        {\textcolor{violet}{\normalsize 1-RTT (HANDSHAKE\_DONE)}};
\end{tikzpicture}

::::

:::

## Handshake: Accepting connection

### TCP

Use **`listen()`** to create a listening socket and use **`accept()`**
to create a socket for incoming connection.

### QUIC

No distinction between listening and connected sockets. Inspect
Connection ID(s) in a received packet to identify connection.

## Handshake: Connection ID

ID used to identify a QUIC connection at en endpoint

Allows the connection to be migrated to other network path, after handshake

### Connection ID negotiation

Two Connection IDs are negotiated for each peer: the **SCID** (Source
Connection ID) and **DCID** (Destination Connection ID)

1. The client randomely generates SCID and DCID
2. The client include them the Initial packet
3. The server randomely generates its own SCID
4. The server includes the new SCID and the client's SCID in the next Initial packet

## Handshake: Accepting connection

\footnotesize
```c
nread = recvmsg (fd, &msg, ...);

ngtcp2_pkt_decode_version_cid (&version,
                               &dcid, &dcidlen,
                               &scid, &scidlen,
                               buf, nread,
                               NGTCP2_MAX_CIDLEN);

/* Find any existing connection matching DCID */
connection = find_connection (..., dcid, dcidlen);
if (!connection)
  {
    ngtcp2_pkt_hd header;
    ngtcp2_accept (&header, data, data_size);

    /* Create a connection */
	connection = ...;
  }
```

## Stream opening

QUIC streams are _implicitly_ opened upon writing.  The following code
opens a bi-directional stream only logically.

\footnotesize
```c
int64_t id;

ngtcp2_conn_open_bidi_stream (conn, &id, NULL);

/* Create a stream */
stream = stream_new (id);

/* Register the stream in connection */
connection_add_stream (connection, stream);
```

## Data transfer

`ngtcp2_conn_writev_stream()` encodes the stream data along with pending
output into `buf` as a UDP payload.

\footnotesize
```c
nwrite = ngtcp2_conn_writev_stream (conn, &path, &pi,
		                            buf, sizeof(buf), &datalen,
		                            NGTCP2_WRITE_STREAM_FLAG_NONE,
		                            stream_id, &datav, 1, ts);

struct iovec iov;
iov.iov_base = (void *)buf;
iov.iov_len = nwrite;
struct msghdr msg;
msg.msg_iov = &iov;
msg.msg_iovlen = 1;

sendmsg (fd, &msg, MSG_DONTWAIT);
```

## Data transfer: Under the hood

::: {.columns}

:::: {.column width="60%"}

- A single UDP packet may contain multiple QUIC packets
- Each QUIC packet may contain multiple QUIC frames

\ 

- `NGTCP2_WRITE_STREAM_FLAG_MORE` allows multiple frames to be coalesced in a single QUIC packet
- The maximum UDP packet size should be determined through PMTUD/DPLPMTUD

::::

:::: {.column}

\begin{tikzpicture}[
  node distance=7mm,
  title/.style={font=\fontsize{6}{6}\color{black!50}\ttfamily},
  typetag/.style={rectangle, draw=black!50, font=\scriptsize\ttfamily, anchor=west}
]
  \node (udp) [title] { UDP packet };

  \node (packet1) [below=of udp.west, title, xshift=2mm] { QUIC packet };
  \node (frame1) [below=of packet1.west, typetag, xshift=2mm] { QUIC frame };
  \node (frame2) [below=of frame1.west, typetag] { QUIC frame };
  \node (frame3) [below=of frame2.west, typetag] { QUIC frame };
  \node (packet1box) [draw=black!50, fit={(packet1) (frame1) (frame2) (frame3)}] {};

  \node (packet2) [right=of packet1.east, title, xshift=2mm] { QUIC packet };
  \node (frame4) [below=of packet2.west, typetag, xshift=2mm] { QUIC frame };
  \node (frame5) [below=of frame4.west, typetag] { QUIC frame };
  \node (frame6) [below=of frame5.west, typetag] { QUIC frame };
  \node (packet2box) [draw=black!50, fit={(packet2) (frame4) (frame5) (frame6)}] {};

  \node (udpbox) [draw=black!50, fit={(udp) (packet1box) (packet2box)}] {};

\end{tikzpicture}

::::

:::

## Data transfer: Frame loss detection and recovery

A packet containing ack-eliciting frames (STREAM, etc) triggers the
receiver to send an ACK frame

ACK frame can include a range of acknowledgement per **packet**

## Putting it all together

- Setting up an `epoll` loop
- Stream data buffering
- Setting up timers

## Setting up an `epoll` loop

Once a socket is created, run the main loop using `epoll_wait()`:

\footnotesize
```c
ev.events = EPOLLIN | EPOLLOUT | EPOLLET;
ev.data.fd = socket_fd;
epoll_ctl (epoll_fd, EPOLL_CTL_ADD, ev.data.fd, &ev);

for (;;)
  {
    int nfds = epoll_wait (epoll_fd, events, MAX_EVENTS, -1);

    for (int n = 0; n < nfds; n++)
	  {
	    if (events[n].data.fd == socket_fd)
		  {
		    if (events[n].events & EPOLLIN)
			  handle_incoming ();
		    if (events[n].events & EPOLLOUT)
			  handle_outgoing ();
          }
	  }
  }
```

## Stream data buffering

Application data written to a stream must be kept until they are
acked.

In the example code, buffering is implemented per stream.  Buffered
data is read in the `epoll` loop when actually writing:

\footnotesize
```c
/* Callback called when stream data is received */
static int
recv_stream_data_cb (ngtcp2_conn *conn, uint32_t flags,
                     int64_t stream_id, uint64_t offset,
					 const uint8_t *data, size_t datalen,
                     void *user_data, void *stream_user_data)
{
  Connection *connection = user_data;
  Stream *stream = connection_find_stream (connection, stream_id);

  if (stream)
    g_queue_push_tail (stream->buffer, g_bytes_new (data, datalen));

  return 0;
}
```

## Setting up timers

For packet loss detection, QUIC also uses timers.

\footnotesize
```c
timer_fd = timerfd_create (CLOCK_MONOTONIC, TFD_NONBLOCK);

ngtcp2_tstamp expiry = ngtcp2_conn_get_expiry (connection->conn);

/* IT is calculated from EXPIRY */
timerfd_settime (connection->timer_fd, 0, &it, NULL);
```

```c
/* Within the epoll loop */
if (events[n].data.fd == timer_fd)
  {
    ngtcp2_conn_handle_expiry (conn, timestamp ());

    /* ngtcp2_conn_writev_stream + sendmsg */
  }
```

## Demo

### Scenarios

- Single stream, single frame per QUIC packet
- Single stream, multiple frames per QUIC packet
  - `--coalescing=N` option
- Multiple streams, single frame per QUIC packet
  - `--streams=M` option

### Traffic capture
```console
SSLKEYLOGFILE=$PWD/keylog.txt ./cli ...
```

```console
tshark -o "tls.keylog_file: $PWD/keylog.txt" \
       -i lo -Px -O quic -Y "udp.port == 5556"
```

## Advanced topics

- Flow control and congestion control
- UDP GSO (Generic Segmentation Offload)

## Flow control and congestion control

Flow control:
  ~ To prevent receiver being overwhelmed

Congestion control:
  ~ To prevent network being congested

## Flow control

- The receiver advertises the limits to the sender, upon handshake
- Those limits can be changed afterwards

\footnotesize

::: {.columns}

:::: {.column width="60%"}

### Initial transport parameters

- initial_max_data
- initial_max_stream_data_bidi_local
- initial_max_stream_data_bidi_remote
- initial_max_stream_data_uni
- initial_max_streams_bidi
- initial_max_streams_uni

::::

:::: {.column}

### Frames

- MAX_DATA
- MAX_STREAM_DATA
- MAX_STREAMS

::::

:::

## Congestion control

- ECN (Explicit Congestion Negotiation)
- TCP's congestion control mechanisms (RFC 5681, ...)
  - Slow start, congestion avoidance, fast retransmit, fast recovery
- Congestion control algorithms

## ECN (Explicit Congestion Negotiation)

1. The sender indicates that it supports ECN in the IP header (ECN-ECT)
2. When an intermediate node (e.g., switch) is overloaded, it sets
   ECN-CE (congestion experienced), and forward the packet
3. The receiver reports the fact that ECN-CE marked packet is
   received, in the upper layer protocol packet (TCP or QUIC)

\begin{tikzpicture}
\draw (0,0) -- (0,3);
\draw (8,0) -- (8,3);

\node[circle,draw,inner sep=1pt,minimum size=1cm] (switch) at (4,1.1) {Switch};
\draw[->] (0,2.8) -- (8,2.8)
	node [above,near start,align=left,font=\small]
	{IP (ECN-ECT)};
\draw[dashed] (0,2) -- (8,2)
	node [above,midway,align=left,font=\small]
	{Congestion};
\draw[->] (0,1.1) -- (switch);
\draw[->] (switch) -- (8,1.1)
	node [above,near start,align=left,font=\small]
	{IP (ECN-CE)};
\draw[<-] (0,0.2) -- (8,0.2)
	node [below,near end,align=left,font=\small]
	{QUIC ACK (ECN-CE Count > 0)};

\end{tikzpicture}

## ECN (Explicit Congestion Negotiation)

Setting ECN bits
  ~ `setsockopt()` with `IPPROTO_IP/IP_TOS` or `IPPROTO_IPV6/IPV6_TCLASS`, or `sendmsg()` with control message

Reading ECN gits
  ~ `IPPROTO_IP/IP_TOS` or `IPPROTO_IPV6/IPV6_TCLASS` control message
  from `recvmsg()`

The `ecn` field in `ngtcp2_pktinfo` can be used to notify the library
about the ECN bits.

## Congestion control algorithms

These algorithms define packet sending strategies when a congestion is
detected, using the techniques such as slow-start, fast retransmit,
and fast recovery.

In TCP:
```sh
$ sysctl net/ipv4/tcp_available_congestion_control
net.ipv4.tcp_available_congestion_control = reno cubic
```

ngtcp2 supports Reno, CUBIC, and BBR, as well as custom implementation.

## UDP GSO (Generic Segmentation Offload)

Kernel mechanism that allows multiple UDP packets to be passed with a
single `sendmsg()` syscall.

\begin{tikzpicture}
\node[rectangle split, rectangle split parts=4, draw, align=center, text width=2.6cm] (udppackets)
{UDP packet
\nodepart{two}
UDP packet
\nodepart{three}
UDP packet
\nodepart{four}
UDP packet
};

\node (kernel) [right=2cm of udppackets.east, draw, inner sep=1cm] { kernel };

\draw[->] (udppackets) to node [above,font=\footnotesize] {sendmsg} (kernel);

\end{tikzpicture}


### Setting GSO segment size

`setsockopt()` with `SOL_UDP` and `UDP_SEGMENT`, or `sendmsg()` with
control message
  
## Takeaways

- QUIC is magical, but not magic
  - The magical logic is in ngtcp2
- QUIC design is largely influenced by TCP
  - Writing a QUIC based application is a great way to learn/refresh TCP/IP knowledge

## Thank you

Questions?

\ 

\footnotesize
- Slides: [https://dueno.gitlab.io/quic-talk/presentation.pdf](https://dueno.gitlab.io/quic-talk/presentation.pdf)
- Demo source: [https://gitlab.com/dueno/quic-echo](https://gitlab.com/dueno/quic-echo)

## Useful resources

### Library reference

- [ngtcp2 documentation](https://nghttp2.org/ngtcp2/index.html)

### RFC

[RFC 8999](https://datatracker.ietf.org/doc/html/rfc8999):
  ~ Version-Independent Properties of QUIC

**[RFC 9000](https://datatracker.ietf.org/doc/html/rfc9000)**:
  ~ **QUIC: A UDP-Based Multiplexed and Secure Transport**

[RFC 9001](https://datatracker.ietf.org/doc/html/rfc9001):
  ~ Using TLS to Secure QUIC

[RFC 9002](https://datatracker.ietf.org/doc/html/rfc9002):
  ~ QUIC Loss Detection and Congestion Control

[draft-ietf-quic-applicability](https://quicwg.org/ops-drafts/draft-ietf-quic-applicability.html):
  ~ Applicability of the QUIC Transport Protocol

### Papers

- Robin Marx, et al.: [Same Standards, Different Decisions: A Study of QUIC and HTTP/3 Implementation Diversity](https://qlog.edm.uhasselt.be/epiq/files/QUICImplementationDiversity_Marx_final_11jun2020.pdf)
- Alexander Yu, et al.: [Dissecting Performance of Production QUIC](https://cs.brown.edu/~tab/papers/QUIC_WWW21.pdf)
