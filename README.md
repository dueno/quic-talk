# quic-talk

Slides for the DevConf.US 2021 talk [Understanding QUIC by examples].

## Building

0. Install pandoc and the dependencies:
```console
$ dnf install make mozilla-fira-mono-fonts mozilla-fira-sans-fonts \
              pandoc texlive-beamer texlive-fira texlive-framed \
              texlive-gsftopk texlive-metafont texlive-mfware texlive-ulem \
              texlive-xetex
```
1. `make`

## License
The MIT License

[Understanding QUIC by examples]: https://devconfus2021.sched.com/event/lkfO/understanding-quic-by-examples
